package com.bakir.stories

import android.app.Application
import com.bakir.stories.di.DaggerAppComponent

class StoriesApp : Application() {

    val appComponent = DaggerAppComponent.create()

}