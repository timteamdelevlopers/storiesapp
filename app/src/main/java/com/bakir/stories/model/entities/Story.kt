package com.bakir.stories.model.entities

data class Story(
    val id: String,
    val previewTitle: String,
    val thumbnail: String?,
    val slides: List<StorySlide>
)