package com.bakir.stories.model.repositories

import com.bakir.stories.model.entities.Story
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StoriesRepository {

    suspend fun loadStories(): List<Story> =
        withContext(Dispatchers.Default) {
            delay(3000)
            val stories = mutableListOf<Story>()
            for (i in 1..10) {
                val story = Story("$i", "Пример текста $1", null, emptyList())
                stories.add(story)
            }
            return@withContext stories
        }
}