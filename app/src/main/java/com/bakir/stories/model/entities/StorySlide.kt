package com.bakir.stories.model.entities

data class StorySlide(
    val id: Int,
    val duration: Int,
    val description: String,
    val imageUrl: String?
)