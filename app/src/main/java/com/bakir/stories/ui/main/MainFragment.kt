package com.bakir.stories.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.bakir.stories.StoriesApp
import com.bakir.stories.databinding.MainFragmentBinding
import com.bakir.stories.di.DaggerMainViewComponent
import com.bakir.stories.ui.stories.*
import javax.inject.Inject

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var binding: MainFragmentBinding
    private lateinit var storiesAdapter: StoriesPreviewAdapter

    @Inject
    lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        setupStoriesList()
        return binding.root
    }

    private fun setupStoriesList() {
        storiesAdapter = StoriesPreviewAdapter { }
        binding.rvStories.apply {
            adapter = storiesAdapter
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appComponent = (requireActivity().application as StoriesApp).appComponent
        DaggerMainViewComponent.factory().create(appComponent).inject(this)

        if (savedInstanceState == null) {
            viewModel.loadStories()
        }

        viewModel.loadingState.observe(
            viewLifecycleOwner,
            Observer { loading -> handleLoadingState(loading) })
        viewModel.storiesResult.observe(
            viewLifecycleOwner,
            Observer { result -> handleStoriesList(result) })
    }

    private fun handleLoadingState(loading: Boolean) {
        binding.pbLoading.isVisible = loading
    }

    private fun handleStoriesList(result: StoriesResult) {
        when (result) {
            is ValidResult -> storiesAdapter.submitList(result.result)
            EmptyResult -> storiesAdapter.submitList(emptyList())
            is ErrorResult -> storiesAdapter.submitList(emptyList())
            TerminalError -> storiesAdapter.submitList(emptyList())
        }
    }

}