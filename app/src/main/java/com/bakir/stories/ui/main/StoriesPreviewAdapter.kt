package com.bakir.stories.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bakir.stories.databinding.ItemStoryPreviewBinding
import com.bakir.stories.model.entities.Story

class StoriesPreviewAdapter(private val listener: (Story) -> Unit) :
    ListAdapter<Story, StoryPreviewViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryPreviewViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemStoryPreviewBinding.inflate(layoutInflater, parent, false)
        return StoryPreviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoryPreviewViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Story>() {
            override fun areItemsTheSame(oldItem: Story, newItem: Story): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Story, newItem: Story): Boolean =
                oldItem == newItem
        }
    }
}