package com.bakir.stories.ui.stories

import com.bakir.stories.model.entities.Story

sealed class StoriesResult
class ValidResult(val result: List<Story>) : StoriesResult()
object EmptyResult : StoriesResult()
class ErrorResult(val e: Throwable) : StoriesResult()
object TerminalError : StoriesResult()