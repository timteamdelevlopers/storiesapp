package com.bakir.stories.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bakir.stories.model.repositories.StoriesRepository
import com.bakir.stories.ui.stories.EmptyResult
import com.bakir.stories.ui.stories.ErrorResult
import com.bakir.stories.ui.stories.StoriesResult
import com.bakir.stories.ui.stories.ValidResult
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(val storiesRepository: StoriesRepository) : ViewModel() {

    private val _mutableStoriesResult = MutableLiveData<StoriesResult>(EmptyResult)
    private val _mutableLoadingState = MutableLiveData<Boolean>(false)

    val storiesResult: LiveData<StoriesResult> get() = _mutableStoriesResult
    val loadingState: LiveData<Boolean> get() = _mutableLoadingState

    fun loadStories() {
        viewModelScope.launch {
            _mutableLoadingState.value = true

            try {
                val result = storiesRepository.loadStories()
                if (result.isEmpty()) {
                    _mutableStoriesResult.value = EmptyResult
                } else {
                    _mutableStoriesResult.value = ValidResult(result)
                }
            } catch (e: Throwable) {
                _mutableStoriesResult.value = ErrorResult(e)
            }

            _mutableLoadingState.value = false
        }
    }

}