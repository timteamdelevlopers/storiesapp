package com.bakir.stories.ui.main

import androidx.recyclerview.widget.RecyclerView
import com.bakir.stories.databinding.ItemStoryPreviewBinding
import com.bakir.stories.model.entities.Story

class StoryPreviewViewHolder(private val binding: ItemStoryPreviewBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(story: Story, listener: (Story) -> Unit) {
        setTitle(story)
        setThumbnail(story)
        setClickListener(listener, story)
    }

    private fun setClickListener(
        listener: (Story) -> Unit,
        movie: Story
    ) {
        itemView.setOnClickListener { listener(movie) }
    }

    private fun setTitle(story: Story) {
        binding.tvTitle.text = story.previewTitle
    }

    private fun setThumbnail(story: Story) {
    }

}