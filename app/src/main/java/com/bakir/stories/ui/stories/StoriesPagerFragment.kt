package com.bakir.stories.ui.stories

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bakir.stories.R

class StoriesPagerFragment : Fragment() {

    companion object {
        fun newInstance() = StoriesPagerFragment()
    }

    private lateinit var viewModel: StoriesPagerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.stories_pager_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(StoriesPagerViewModel::class.java)
    }

}