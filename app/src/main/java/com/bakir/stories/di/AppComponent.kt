package com.bakir.stories.di

import com.bakir.stories.model.repositories.StoriesRepository
import dagger.Component

@Component(modules = [MainModule::class])
interface AppComponent {

    fun getStoriesRepository(): StoriesRepository

}