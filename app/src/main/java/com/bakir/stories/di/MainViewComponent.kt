package com.bakir.stories.di

import com.bakir.stories.ui.main.MainFragment
import dagger.Component

@Component(dependencies = [AppComponent::class])
interface MainViewComponent {

    @Component.Factory
    interface Factory {
        fun create(appComponent: AppComponent): MainViewComponent
    }

    fun inject(fragment: MainFragment)
}