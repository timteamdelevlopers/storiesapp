package com.bakir.stories.di

import com.bakir.stories.model.repositories.StoriesRepository
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun provideStoriesRepository(): StoriesRepository {
        return StoriesRepository()
    }

}